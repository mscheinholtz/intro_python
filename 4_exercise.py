def sorter(key):
    "simply returns the value"
    return players[key][1]

players = {185:('Tyler', 'Chatwood'),   
           219:('Luke', 'Farrell'),  
           190:('Kyle', 'Hendricks')}

keylist = list(players.keys())
keylist.sort(key=sorter)

fmt= "{last:<15} {first:<15} {weight:<10}"
print(fmt.format(last="last", first="first", weight="weight"))
dashes="-"*8
print(fmt.format(first=dashes, last=dashes, weight=dashes))
for key in keylist:
    first, last = players[key]
    print(fmt.format(first=first, last=last, weight=key))

print("***************************************************")
players = {'Hendricks': ('Kyle', 225),
           'HeNDricks': ('Kyle', 225),
           'Hendrix': ('Kyle', 225) }

updated_keys = set()
updated_dict = dict()

#test_set = set(["aaa", "AAA", "abA", "AaA"])
for last_name in players:
    #print("Original Last name: {}".format(last_name) + "\tLowercase Last name: {}".format(last_name).lower())
    #print("\tFirst name: {}".format(players[last_name][0]) + "\tWeight: {}".format(players[last_name][1]))
    element_ct = len(updated_keys)
    first, weight = players[last_name]
    updated_keys.add(last_name.lower())
    if len(updated_keys) > element_ct:  # compare "before" length to "after"
        updated_dict[last_name.lower()] = (first, weight)    
    
print(updated_keys)

for key in updated_dict.keys():
    first, weight = updated_dict[key]
    print(fmt.format(first=first, last=key, weight=weight))

print(updated_dict)