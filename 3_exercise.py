import keyword

python_keywords = [x.lower() for x in keyword.kwlist]
count_correctly_guessed_keywords = 0
correctly_guessed_keywords = []
incorrectly_guessed_keywords = []

guess = input("Guess a Python keyword: ").lower()
while guess != "help":
    if guess in python_keywords:
        if guess in correctly_guessed_keywords:
            print("Already guessed that keyword. Try again")
            guess = input("Guess a Python keyword: ").lower()
        else:
            print("You guessed correct!")
            count_correctly_guessed_keywords = (count_correctly_guessed_keywords + 1)
            correctly_guessed_keywords.append(guess)
            if count_correctly_guessed_keywords == len(python_keywords):
                print("You guess all the keywords")
                guess="help"
            else:
                guess = input("Guess a Python keyword: ").lower()
    else:
        print("Incorrect guess!!!!!")
        incorrectly_guessed_keywords.append(guess)
        guess = input("Guess a Python keyword: ").lower()

print("Count of correctly guessed keywords: {}".format(count_correctly_guessed_keywords))
print("Keywords guessed correct: {}".format(correctly_guessed_keywords))
print("Count of keywords not guessed: {}".format(len(python_keywords) - count_correctly_guessed_keywords))
print("Keywords guessed incorrect: {}".format(incorrectly_guessed_keywords))