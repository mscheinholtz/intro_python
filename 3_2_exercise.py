import random
min_range_random_number = 1
max_range_random_number = 100
random_number = random.randint(min_range_random_number,max_range_random_number)
print("Random Number: {}".format(random_number))
for i in range(0,5):
    try:
        guess = int(input("Guess number between {}".format(min_range_random_number) + " and {}".format(max_range_random_number) + ": "))
    except:
        print("Enter whole numbers only")
    if guess >= min_range_random_number and guess <= max_range_random_number:
        if guess == random_number:
            print("Guess correct!")
            break
        else:
            print("Incorrect guess")
    else:
        print("Guess a number in the range {}".format(min_range_random_number) + " - {}".format(max_range_random_number))

